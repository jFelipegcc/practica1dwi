package mx.edu.uteq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class LomichisMain {

	public static void main(String[] args) {
		SpringApplication.run(LomichisMain.class, args);
	}

}
